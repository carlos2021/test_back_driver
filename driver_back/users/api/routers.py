from rest_framework.routers import DefaultRouter

from driver_back.users.api.api import UserViewSet

router = DefaultRouter()

router.register("", UserViewSet, basename="users")

urlpatterns = router.urls
