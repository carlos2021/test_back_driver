import math


def distance(x1, x2, y1, y2):
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def driver_nearest(instances_driver, lat, lon, datetime_search):
    list_with_order = []
    for instance in instances_driver:
        list_with_order.append(
            {
                "distance": distance(
                    instance.work_latitude, lat, instance.work_length, lon
                ),
                "instance": instance,
            }
        )
    list_sorted = sorted(list_with_order, key=lambda instance: instance["distance"])
    return list_sorted[0]["instance"]
