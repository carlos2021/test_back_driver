from rest_framework.routers import DefaultRouter

from driver_back.driver.api.viewsets.driverViewset import DriverViewSet
from driver_back.driver.api.viewsets.orderViewset import OrderViewSet


router = DefaultRouter()

router.register("order", OrderViewSet, basename="order")
router.register("driver", DriverViewSet, basename="driver")

urlpatterns = router.urls
