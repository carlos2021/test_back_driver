from rest_framework import serializers
from driver_back.driver.models import Order

from driver_back.driver.utilites import is_availability_of_driver


class OrderSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if data["collected_latitude"] < 0 or data["collected_latitude"] > 100:
            raise serializers.ValidationError(
                "Introduzca un rago valido de en la latitud de origen(0-100)"
            )

        if data["collected_length"] < 0 or data["collected_length"] > 100:
            raise serializers.ValidationError(
                "Introduzca un rago valido de en la longitud de origen(0-100)"
            )

        if data["destination_length"] < 0 or data["destination_length"] > 100:
            raise serializers.ValidationError(
                "Introduzca un rago valido de en la longitud de destino(0-100)"
            )

        if data["destination_latitude"] < 0 or data["destination_latitude"] > 100:
            raise serializers.ValidationError(
                "Introduzca un rago valido de en la latitud de destino(0-100)"
            )
        if is_availability_of_driver(data["driver"], data["scheduling_date"]):
            raise serializers.ValidationError(
                "Este conductor a esta hora no esta disponible"
            )
        return data

    class Meta:
        model = Order
        fields = "__all__"
