from django.db import models


class Driver(models.Model):
    alfred = models.IntegerField(unique=True)
    work_latitude = models.IntegerField(blank=True, null=True)
    work_length = models.IntegerField(blank=True, null=True)
    last_update = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = "Conductor"
        verbose_name_plural = "Conductores"


class Order(models.Model):
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    scheduling_date = models.DateTimeField()
    collected_latitude = models.IntegerField()
    collected_length = models.IntegerField()
    destination_length = models.IntegerField()
    destination_latitude = models.IntegerField()

    class Meta:
        verbose_name = "Pedido"
        verbose_name_plural = "Pedidos"
